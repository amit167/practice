def life(m):  # function
    result = []  # list of lines
    for row in range(len(m)):  # for loop every row
        line = []  # list per line
        for num in range(len(m[row])):  # for loop every subject
            n = 0  # counter
            if num != len(m[0]) - 1:  # if not last/first
                if m[row][num + 1] == 1:  # if neighbor alive
                    n += 1  # count +1
            if num != 0:
                if m[row][num - 1] == 1:
                    n += 1
            if row != 0:
                if m[row - 1][num] == 1:
                    n += 1
            if row != 0 and num != 0:
                if m[row - 1][num - 1] == 1:
                    n += 1
            if row != 0 and num != (len(m[0]) - 1):
                if m[row - 1][num + 1] == 1:
                    n += 1
            if row != len(m) - 1:
                if m[row + 1][num] == 1:
                    n += 1
            if row != len(m) - 1 and num != 0:
                if m[row + 1][num - 1] == 1:
                    n += 1
            if row != len(m) - 1 and num != len(m[0]) - 1:
                if m[row + 1][num + 1] == 1:
                    n += 1
            if ((n == 2 or n == 3) and m[row][num] == 1) or (n == 3 and m[row][num] == 0):  # if should be alive
                line.append(1)  # add alive
            else:  # if shouldn't be alive
                line.append(0)  # add dead
        result.append(line)  # add line
    return result  # answer


def render(m):
    return "\n".join("".join("O" if b else "." for b in row) for row in m)


board = [
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
]

print(render(board))
print("-" * 20)

expected1 = [
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
]

result1 = life(board)
print(render(result1))
assert result1 == expected1

print("-" * 20)

expected2 = [
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
]

result2 = life(result1)
print(render(result2))
assert result2 == expected2

print("OK")
